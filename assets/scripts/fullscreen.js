const fs = () => {
  const $el = document.documentElement;
  if ($el.requestFullscreen) $el.requestFullscreen();
  else if ($el.mozRequestFullScreen) $el.mozRequestFullScreen();
  else if ($el.webkitRequestFullscreen) $el.webkitRequestFullscreen();
  else if ($el.msRequestFullscreen) $el.msRequestFullscreen();
};

document.querySelector('.fullscreenBtn').addEventListener('click', () => fs());
